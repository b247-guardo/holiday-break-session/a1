const express = require("express");
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended : true}));


const books = [
	{ title: 'No Logo', author: 'Naomi Klein', pages: 490 },
  	{ title: 'North', author: 'Seamus Heaney', pages: 304},
	{ title: 'To Kill a Mockingbird', author: 'Harper Lee', pages: 281 },
  	{ title: 'The Great Gatsby', author: 'F. Scott Fitzgerald', pages: 180 },
  	{ title: 'Silent Spring', author: 'Rachel Carson', pages: 400 },
  	{ title: 'Noli Me Tángere', author: 'José Rizal', pages: 438 },
  	{ title: '1984', author: 'George Orwell', pages: 328 },
  	{ title: 'The Sixth Extinction', author: 'Elizabeth Kolbert', pages: 391 },
  	{ title: 'Brave New World', author: 'Aldous Huxley', pages: 311 },
  	{ title: 'The Catcher in the Rye', author: 'J.D. Salinger', pages: 234 },

  	{ title: 'The Catcher in the Rye', author: 'J.D. Salinger', pages: 234 },

];

const total_pages = books.reduce((total, book) => total + book.pages, 0);

app.get("/total-pages", (request, response) => {
	response.send(`The total number of pages is ${total_pages}.`);
});

const large_books = books.filter(book => {
	if (book.pages >= 250){
		return true;
	}
	else {
		return false;
	}
});

app.get("/large-books", (request, response) => {
	response.send(large_books);
})

app.get("/most-prolific-author", (request, response) => {

	const authors = {};
	let most_prolific_authors = '';
	let most_prolific_author_pages = 0;

	books.forEach(book => {
		if (authors[book.author]) {
			authors[book.author] += book.pages;
		} else {
			authors[book.author] = book.pages;
		}
	});

	for (let author in authors) {
		if (authors[author] > most_prolific_author_pages){
			most_prolific_authors = author;
			most_prolific_author_pages = authors[author];
		}
	}

	response.send(`${most_prolific_authors} is the most prolific author with ${most_prolific_author_pages} pages written.`);
});

app.listen(port, () => {
	console.log(`Server is running at localhost: ${port}`)
});

